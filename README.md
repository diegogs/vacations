# README

## Getting started

The application is developed with Docker, so a simple `docker-compose up`
should be enough to get it up and running. Tests can be run by `docker-compose
run web rspec`.

If docker is not available, you will need a recent ruby version (2.5
recommended), then simply

```{.sh}
bundle
rails s
```

Once you use any of those methods you will find the app in
<http://localhost:3000>


## Usage

Here are some sample queries using `curl`

```{.sh}
curl -L http://localhost:3000/locations
curl -L 'http://localhost:3000/locations?category=cultural&location=outdoors'
curl -L 'http://localhost:3000/recommendations?category=shopping&start_timestamp=2018-08-25T00:00:00+02:00&end_timestamp=2018-08-25T15:00:00+02:00'
```

## Design and tradeoffs

The app is built as a Rails API project, just because I'm already familiar with
the technology.

As far as general design, I've tried to balance simplicity with extensibility,
keeping in mind that the instructions say "think of it as the first milestone
in a bigger project". Looking at the possible extensions and by my own
experience, my main concern is coupling the code too closely with the
storage and format of the data, I don't think a simple JSON file will be
enough once the application grows a bit.

-   I decided to parse the given JSON into a small object hierarchy, with
    `Location` at its root. While it would have been possible to just use the
    hashes and arrays returned by `JSON.parse`, we would be tied to that format
    not just as our input, but as our working data structures. Any changes in
    the JSON file could mean having to rewrite not just parsing, but actual
    business logic.
-   I also isolated the access to the data behind a Repository. This way, any
    changes in the way the data is retrieved can be accommodated. This could
    come in handy for example with the "adding more cities" extension (more on
    that later...). It also proved to be useful when filtering the
    activities for both endpoints.
-   For the GeoJSON serialization, since the format is somewhat different from
    the data layout for my objects, I went ahead with a custom made Serializer,
    since Rails' standard way expects your models to be very similar to the
    output format.
-   I also created a small service for the recommendations operation. I did
    this mostly to have an easier time testing the code without having to mock
    or stub anything.


Other than that, and the fact that there's no database backed models, the
application is pretty standard Rails with RSpec tests. The testing is done at
the request level to have some end to end tests for both operations, and more
exhaustively at the unit level.


## Extensions

-   No outdoor activities on rainy days: I would do a `MeteoRepository`, with a
    `weather_at(date,   location)` method, and provide this to the
    `RecommendationService` as an initialization parameter. Then I would simply
    check the weather for the possible recommendations that we find before
    returning the best one. Again, hiding how we get the data in a `Repository`
    would allow us many possible implementations for the actual data fetching:
    Check every time a query is made against an API, possibly adding a caching
    layer of some sort, have a background process that updates the weather in a
    reasonable timeframe (every hour, day?) in a database and then have this
    Repository read from there... For the relationship between weather data and
    locations I'd use something similar to the bounding boxes idea from the
    cities extension (it would of course depend on what the weather data looks
    like)
-   Multiple cities: I would add a second source of information with city
    coordinates or bounding polygons, since we already have the coordinates for
    each activity in our current data set. If we go this way it may also make
    sense to move both datasets (activities and cities) to a database where we
    can query them together, this way we would only need to change the
    `LocationRepository`. If we wanted to keep the activities in a separate
    file, or in any case use different stores, we'd need to add some more code
    (a new `LocationQueryService` for example) to filter by city before or
    after filtering by other criteria. We'd also have to think about how we
    want the endpoints to behave, mainly if filtering by city is mandatory or
    not.
-   Filling the time range: This is a scheduling problem, it's similar to the
    <https://en.wikipedia.org/wiki/Knapsack_problem>, so I would first explore
    algorithms in this space. We'd also need to keep in mind other
    considerations: time to get from one activity to the next, sleep, eat or
    rest time... In any case, for a first approximation of something that could
    work, I would use the current implementation, but instead of taking the
    longest activity I'd take the one that ends earliest. Then I'd remove it
    from the pool of activities, and repeat the process, changing the start of
    the date range to be the end of the selected activity + some travel time.
    Then repeat until the date range or activities are empty, returning the
    selected activities. The format for the returned response would also have
    to include some sort of time information for each location.


