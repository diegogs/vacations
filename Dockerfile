FROM ruby:2.5

RUN apt-get update -qq && apt-get install -y build-essential nodejs

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY Gemfile /usr/src/app/
COPY Gemfile.lock /usr/src/app/
RUN bundle install

