# frozen_string_literal: true

Rails.application.routes.draw do
  resources :recommendations, only: :index
  resources :locations, only: :index
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
