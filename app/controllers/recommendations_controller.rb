# frozen_string_literal: true

class RecommendationsController < ApplicationController

  # GET /recommendations
  def index
    location = best_location
    if location
      serializer = LocationGeoJsonSerializer.new(location)
      render json: serializer
    else
      render json: { error: 'Not found' }, status: :not_found
    end
  rescue ArgumentError
    render json: { error: 'Invalid dates' }, status: :unprocessable_entity
  end

  private

  def best_location
    repository = LocationRepository.for_activities_file

    service = RecommendationService.new(location_repository: repository)
    service.best_recommendation(range_start, range_end, category: params[:category])
  end

  def range_start
    Time.parse(params[:start_timestamp])
  end

  def range_end
    Time.parse(params[:end_timestamp])
  end

end
