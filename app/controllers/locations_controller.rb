# frozen_string_literal: true

class LocationsController < ApplicationController

  # GET /locations
  def index
    repository = LocationRepository.for_activities_file
    filtered_locations = repository.query(query_params)

    render json: LocationsGeoJsonSerializer.new(filtered_locations)
  end

  private

  def query_params
    params
      .permit(:category, :location, :district)
      .to_h
      .symbolize_keys
  end

end
