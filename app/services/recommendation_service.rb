# frozen_string_literal: true

class RecommendationService

  def initialize(location_repository:)
    @location_repository = location_repository
  end

  def best_recommendation(range_start, range_end, query)
    available_locations = location_repository.query(query).select do |location|
      location.available_in_range?(range_start, range_end)
    end

    available_locations.sort_by(&:hours_spent).last
  end

  private

  attr_reader :location_repository

end
