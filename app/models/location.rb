# frozen_string_literal: true

# :reek:TooManyInstanceVariables: Model object with attributes
class Location

  def self.from_json(payload)
    payload = payload.deep_symbolize_keys
    payload[:opening_hours] = payload[:opening_hours].transform_values do |hours|
      OpeningHours.from_json(hours)
    end
    payload[:coordinates] = Coordinates.from_json(payload[:latlng])
    new(payload.except(:latlng))
  end

  attr_reader :name, :hours_spent, :opening_hours, :coordinates, :other_properties

  def initialize(name:, hours_spent:, opening_hours:, coordinates:, **other_properties)
    @name = name
    @hours_spent = hours_spent
    @opening_hours = opening_hours
    @coordinates = coordinates
    @other_properties = other_properties
  end

  def matches?(query)
    query.all? do |prop_name, prop_value|
      other_properties[prop_name] == prop_value
    end
  end

  def available_in_range?(range_start, range_end)
    daily_ranges_for_availability_query(range_start, range_end).any? do |daily_start, daily_end|
      available_in_same_day_range?(daily_start, daily_end)
    end
  end

  private

  def available_in_same_day_range?(daily_start, daily_end)
    opening_hours = opening_hours_for_date(daily_start)
    open_start, open_end = *opening_hours.ranges_for_date(daily_start)

    return false unless open_start && open_start <= daily_end && open_end >= daily_start

    max_seconds_in_location = [open_end, daily_end].min - [open_start, daily_start].max

    max_hours_in_location = max_seconds_in_location / 3_600.0

    max_hours_in_location >= hours_spent
  end

  # Since the schedules are weekly, we only need to return at most 9 days, for
  # example Monday to Tuesday. Once we are sure we are seeing a full day for
  # the second time (Monday can be partial in the example, Tuesday would be the
  # first full one), we can be sure every possibility is covered

  def daily_ranges_for_availability_query(range_start, range_end)
    ranges = (range_start.to_date..range_end.to_date).take(9).map do |date|
      [date.beginning_of_day, date.end_of_day]
    end
    return [] if ranges.empty?
    ranges.first[0] = range_start
    ranges.last[1] = range_end if ranges.length < 9
    ranges
  end

  def opening_hours_for_date(date)
    key = date.strftime('%a').slice(0..1).downcase.to_sym
    opening_hours[key]
  end

end
