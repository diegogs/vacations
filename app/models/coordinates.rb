# frozen_string_literal: true

class Coordinates

  def self.from_json(expression)
    latitude, longitude = *expression

    new(latitude: latitude, longitude: longitude)
  end

  attr_reader :latitude, :longitude

  def initialize(latitude:, longitude:)
    @latitude = latitude
    @longitude = longitude
  end

end
