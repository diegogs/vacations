# frozen_string_literal: true

class LocationRepository

  def self.for_activities_file
    locations_json = JSON.parse(File.read(ENV['ACTIVITIES_PATH']))
    locations = locations_json.map { |payload| Location.from_json(payload) }

    LocationRepository.new(locations)
  end

  def initialize(locations)
    @locations = locations
  end

  def query(query_params)
    locations.select do |location|
      location.matches?(query_params)
    end
  end

  private

  attr_reader :locations

end
