# frozen_string_literal: true

class OpeningHours

  def self.from_json(expression)
    hours_string = expression.first || ''
    opens, closes = *hours_string.split('-')
    new(opens: opens, closes: closes)
  end

  attr_reader :opens, :closes

  def initialize(opens:, closes:)
    @opens = opens
    @closes = closes
  end

  def ranges_for_date(date)
    return [] unless opens
    date_part = date.strftime('%Y/%m/%d')

    [Time.parse("#{date_part} #{opens}"),
     Time.parse("#{date_part} #{closes}")]
  end

end
