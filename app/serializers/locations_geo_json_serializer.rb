
# frozen_string_literal: true

class LocationsGeoJsonSerializer

  def initialize(locations)
    @locations = locations
  end

  def as_json(*)
    {
      type: 'FeatureCollection',
      features: locations.map do |location|
        LocationGeoJsonSerializer.new(location).as_json
      end
    }
  end

  private

  attr_reader :locations

end
