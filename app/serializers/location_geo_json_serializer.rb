
# frozen_string_literal: true

# :reek:FeatureEnvy: It's a collaborator

class LocationGeoJsonSerializer

  def initialize(location)
    @location = location
  end

  def as_json(*)
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: coordinates
      },
      properties: properties
    }
  end

  private

  attr_reader :location

  def coordinates
    coordinates = location.coordinates

    [coordinates.longitude, coordinates.latitude]
  end

  def properties
    opening_hours_properties = location.opening_hours.transform_values do |hours|
      { opens: hours.opens, closes: hours.closes }
    end
    { name: location.name, opening_hours: opening_hours_properties }.merge(location.other_properties)
  end

end
