# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RecommendationService do

  it "returns nil when it can't find a suitable recommendation" do
    repository = LocationRepository.new([])
    service = RecommendationService.new(location_repository: repository)

    location = service.best_recommendation(
      Time.new(2017, 1, 1), Time.new(2017, 2, 1), category: 'cultural'
    )

    expect(location).to be_nil
  end

  it 'returns the longest recommendation for those it filters' do
    repository = LocationRepository.new(all_locations)
    service = RecommendationService.new(location_repository: repository)

    location = service.best_recommendation(
      Time.new(2017, 1, 1), Time.new(2017, 2, 1), category: 'cultural'
    )

    expect(location.name).to eq('Museo Nacional del Prado')
  end

  it "doesn't break if the given date range is huge" do
    repository = LocationRepository.new(all_locations)
    service = RecommendationService.new(location_repository: repository)

    expect do
      Timeout.timeout(5) do
        service.best_recommendation(
          Time.new(0, 1, 1), Time.new(20_000_000_000_000_000, 2, 1), category: 'cultural'
        )
      end
    end.to_not raise_error

  end

end
