# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Location do

  before :each do
    locations_json = JSON.parse(File.read(ENV['ACTIVITIES_PATH']))
    @location = Location.from_json(locations_json.first)
  end

  describe '.from_json' do

    it 'extracts the basic fields from the given JSON structure' do
      expect(@location.name).to eq('El Rastro')
      expect(@location.hours_spent).to eq(2.5)
    end

    it 'parses the opening hours into their own object' do
      expect(@location.opening_hours[:su].opens).to eq('09:00')
    end

    it 'parses latitude and longitude into their own object' do
      expect(@location.coordinates.latitude).to eq(40.4087357)
    end

    it 'saves other properties in a hash' do
      expect(@location.other_properties.keys).to match(%i[category location district])
    end
  end

  describe '#available_in_range?' do
    it 'is not when the location is closed in those hours' do
      sunday_late = Time.new(2018, 8, 19, 19, 0)
      sunday_night = Time.new(2018, 8, 19, 23, 59)

      expect(@location.available_in_range?(sunday_late, sunday_night)).to be_falsy
    end

    it "is when the location is open during the range and there's time for a visit" do
      sunday_morning = Time.new(2018, 8, 19).beginning_of_day
      sunday_night = Time.new(2018, 8, 19).end_of_day

      expect(@location.available_in_range?(sunday_morning, sunday_night)).to be_truthy
    end

    it "is not when the location's visit time won't allow for a visit in the range" do
      almost_closed = Time.new(2018, 8, 19, 14, 45)
      closed = Time.new(2018, 8, 19, 15, 0)

      expect(@location.available_in_range?(almost_closed, closed)).to be_falsy
    end

    it "is not when the visit would take more time than it's available in the given range" do
      sunday_10am = Time.new(2018, 8, 19, 10, 0)
      sunday_11am = Time.new(2018, 8, 19, 11, 0)

      expect(@location.available_in_range?(sunday_10am, sunday_11am)).to be_falsy
    end

    it "is when there's enough time for a visit" do
      sunday_morning = Time.new(2018, 8, 19).beginning_of_day
      sunday_enough = Time.new(2018, 8, 19, 12, 15)

      expect(@location.available_in_range?(sunday_morning, sunday_enough)).to be_truthy
    end

    it "is for multi-day ranges if it's available on one day" do
      monday_morning = Time.new(2018, 8, 13).beginning_of_day
      sunday_enough = Time.new(2018, 8, 19, 12, 15)

      expect(@location.available_in_range?(monday_morning, sunday_enough)).to be_truthy
    end

    it "is not for multi day ranges starting and ending on the same day that don't include the activity" do
      sunday_start = Time.new(2018, 8, 12, 17, 30)
      next_sunday_end = Time.new(2018, 8, 19, 8, 30)

      expect(@location.available_in_range?(sunday_start, next_sunday_end)).to be_falsy
    end

  end

end
