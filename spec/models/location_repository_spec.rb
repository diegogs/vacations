# frozen_string_literal: true

require 'rails_helper'

RSpec.describe LocationRepository do

  before :each do
    @repository = LocationRepository.new(all_locations)
  end

  describe '#query' do
    it 'returns all the locations when the query is empty' do
      expect(@repository.query({}).length).to eq(10)
    end

    it 'returns only those locations specified by the given query' do
      expect(@repository.query(location: 'indoors', category: 'shopping').length).to eq(1)
    end

    it 'returns no locations when none match the query' do
      expect(@repository.query(location: 'NOP', category: 'NOP')).to be_empty
    end
  end

end
