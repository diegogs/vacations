# frozen_string_literal: true

require 'rails_helper'

RSpec.describe LocationsGeoJsonSerializer do
  before :each do
    @serializer = LocationsGeoJsonSerializer.new(all_locations)
  end

  it 'wraps the given locations geo json' do
    geo_json = @serializer.as_json

    expect(geo_json[:type]).to eq('FeatureCollection')
    expect(geo_json[:features].length).to eq(10)
  end

end
