# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Location do

  before :each do
    @serializer = LocationGeoJsonSerializer.new(all_locations.first)
  end

  describe '#as_json' do
    it 'returns its basic coordinates' do
      geo_json = @serializer.as_json

      expect(geo_json[:type]).to eq('Feature')
      expect(geo_json[:geometry][:coordinates].last).to eq(40.4087357)
    end

    it 'serializes the rest of the properties' do
      properties = @serializer.as_json[:properties]

      expect(properties[:name]).to eq('El Rastro')
      expect(properties[:category]).to eq('shopping')
      expect(properties[:opening_hours][:mo]).to eq(opens: nil, closes: nil)
      expect(properties[:opening_hours][:su]).to eq(opens: '09:00', closes: '15:00')
    end

  end

end
