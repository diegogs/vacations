# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Recommendations', type: :request do
  describe 'GET /recommendations' do
    it 'gets the best recommendation for a given query' do
      get recommendations_path(
        format: :json,
        category: 'shopping',
        start_timestamp: '2018-08-25T00:00:00+02:00',
        end_timestamp: '2018-08-25T15:00:00+02:00'
      )
      body = JSON.parse(response.body)

      expect(body['properties']['name']).to eq 'ECI Nuevos Ministerios'
    end

    it 'returns a nice error when no recommendation works' do
      get recommendations_path(
        format: :json,
        category: 'nothing will match me',
        start_timestamp: '2018-08-25T00:00:00+02:00',
        end_timestamp: '2018-08-25T15:00:00+02:00'
      )
      expect(response.code).to eq('404')

      body = JSON.parse(response.body)

      expect(body['error']).to eq 'Not found'
    end

    it 'returns a nice error when the dates given are not parseable' do
      get recommendations_path(
        format: :json,
        category: 'shopping',
        start_timestamp: 'nooop',
        end_timestamp: 'not parsing this as date'
      )
      expect(response.code).to eq('422')

      body = JSON.parse(response.body)

      expect(body['error']).to eq 'Invalid dates'

    end
  end
end
