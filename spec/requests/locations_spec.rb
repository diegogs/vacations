# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Locations', type: :request do
  describe 'GET /locations' do
    it 'gets all the locations when they no other params are given' do
      get locations_path(format: :json)

      body = JSON.parse(response.body)

      expect(body['features'].length).to eq 10
    end

    it 'gets them in GeoJSON format' do
      get locations_path(format: :json)

      body = JSON.parse(response.body)
      location_name = body['features'].first['properties']['name']

      expect(location_name).to eq 'El Rastro'
    end

    it 'filters the results by category' do
      get locations_path(format: :json, category: 'shopping')

      body = JSON.parse(response.body)

      expect(body['features'].length).to eq 3
    end
  end
end
